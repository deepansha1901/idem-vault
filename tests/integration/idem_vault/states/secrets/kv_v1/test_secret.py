import pytest
import yaml
from pytest_idem.runner import idem_cli
from pytest_idem.runner import run_yaml_block

# Parametrization options for running each test with --test first and then without --test
PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

SECRET_PATH = "secret/idem_test"

PRESENT_STATE = f"""
test_resource:
  vault.secrets.kv_v1.secret.present:
    - path: {SECRET_PATH}
    - data:
        mapping:
          key: value
""".rstrip()

ABSENT_STATE = f"""
test_resource:
  vault.secrets.kv_v1.secret.absent
""".rstrip()

SEARCH_STATE = f"""
{PRESENT_STATE}
verify:
  vault.secrets.kv_v1.secret.search:
    - resource_id: ${{vault.kv_v1.secret:test_resource:resource_id}}
"""


@pytest.mark.parametrize(**PARAMETRIZE)
def test_starting_absent(esm_cache, acct_data, __test, version):
    """
    Verify that the absent state is successful for a resource that never existed
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])

    assert resource_ret["new_state"] is None


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(hub, ctx, esm_cache, acct_data, __test, version):
    """
    Create a brand new secret based on an image that exists locally
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_no_update", depends=["present"])
@pytest.mark.asyncio
async def test_present_no_update(hub, ctx, esm_cache, acct_data, __test, version):
    """
    Create a brand new secret based on an image that exists locally
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["old_state"] and resource_ret["new_state"]
    assert resource_ret["old_state"] == resource_ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present_no_update"])
def test_already_present(esm_cache, acct_data, __test, version):
    """
    The secret should exist in the cache, verify that the resource id gets populated and no changes are made
    """
    if version == "v2":
        return
        # Run the state defined in the yaml bloc    if version == "v2":
        returnk
    ret = run_yaml_block(
        PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])

    if not __test:
        assert not resource_ret["changes"], resource_ret["changes"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present_no_update"])
def test_describe(esm_cache, acct_data, __test, version):
    """
    Describe all secrets and run the "present" state the described secret created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    if version == "v2":
        return

    # Describe all secrets
    ret = idem_cli("describe", "vault.secrets.kv_v1.secret", acct_data=acct_data)

    # Get the state for our new secret, that was created by describe
    described_states = yaml.safe_dump(ret)

    # Run the present state for our resource created by describe
    ret = run_yaml_block(
        described_states,
        managed_state=esm_cache,
        acct_data=acct_data,
        test=__test,
    )
    for resource_ret in ret.values():
        assert resource_ret["result"], "\n".join(resource_ret["comment"])
        # No changes should have been made!
        # We just created this state from describe
        assert not resource_ret["changes"], resource_ret["changes"]


# TODO Make a change, verify changes happen


# Cleanup time


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["present_no_update"])
def test_absent(esm_cache, acct_data, __test, version):
    """
    Terminate the secret
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["new_state"] is None


@pytest.mark.parametrize(**PARAMETRIZE)
def test_already_absent(esm_cache, acct_data, __test, version):
    """
    Verify that termination is idempotent
    """
    if version == "v2":
        return
    # Run the state defined in the yaml block
    ret = run_yaml_block(
        ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data, test=__test
    )
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], resource_ret["comment"]
    assert resource_ret["new_state"] is None
